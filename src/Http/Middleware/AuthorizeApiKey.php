<?php

namespace Trungnq\ApiKey\Http\Middleware;

use Closure;
use Trungnq\ApiKey\Models\ApiKey;
use Trungnq\ApiKey\Models\ApiKeyAccessEvent;
use Illuminate\Http\Request;


class AuthorizeApiKey
{
    const AUTH_HEADER = 'X-Authorization';

    public function handle(Request $request, Closure $next)
    {
        $apiKey = ApiKey::getByKey($request->header(self::AUTH_HEADER));

        if ($apiKey instanceof ApiKey) {
            $this->logAccessEvent($request, $apiKey);
            return $next($request);
        }

        return response([
            'errors' => [[
                'message' => 'Unauthorized'
            ]]
        ], 401);
    }

    protected function logAccessEvent(Request $request, ApiKey $apiKey)
    {
        $event = new ApiKeyAccessEvent;
        $event->api_key_id = $apiKey->id;
        $event->ip_address = $request->ip();
        $event->url        = $request->fullUrl();
        $event->save();
    }
}
