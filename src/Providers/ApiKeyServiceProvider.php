<?php

namespace Trungnq\ApiKey\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Trungnq\ApiKey\Console\Commands\DeleteApiKey;
use Trungnq\ApiKey\Console\Commands\GenerateApiKey;
use Trungnq\ApiKey\Console\Commands\ListApiKeys;
use Trungnq\ApiKey\Http\Middleware\AuthorizeApiKey;

class ApiKeyServiceProvider extends ServiceProvider
{
    public function boot(Router $router)
    {
        $this->registerMiddleware($router);
        $this->registerMigrations(__DIR__ . '/../../database/migrations');
    }

    public function register()
    {
        $this->commands([
            DeleteApiKey::class,
            GenerateApiKey::class,
            ListApiKeys::class,
        ]);
    }

    /**
     * Register middleware
     *
     * Support added for different Laravel versions
     *
     * @param Router $router
     */
    protected function registerMiddleware(Router $router)
    {
        $versionComparison = version_compare(app()->version(), '5.4.0');

        if ($versionComparison >= 0) {
            $router->aliasMiddleware('auth.apikey', AuthorizeApiKey::class);
        } else {
            $router->middleware('auth.apikey', AuthorizeApiKey::class);
        }
    }

    /**
     * Register migrations
     */
    protected function registerMigrations($path)
    {
        $this->publishes([
            $path => database_path('migrations')
        ], 'migrations');
    }
}
